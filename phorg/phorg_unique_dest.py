"""Tool for renaming duplicate files."""
import phorg
import logging
import os
import re

LOGGER = logging.getLogger(__name__)


def get_unique_dest(dest):
    """Get a unique path for this destination. If it doesn't exist then
    there's nothing to do. Otherwise insert a counter into the file
    name. Throw an error if we go over 100 duplicates"""
    do_warn = False
    orig_dest = dest
    while os.path.exists(dest):
        do_warn = True
        match = re.fullmatch(r'(.*-)([0-9][0-9])(.[^.]*)', dest)
        if not match:
            dest_split = dest.split('.')
            prefix = '.'.join(dest_split[:-1])+'-'
            counter = 1
            suffix = '.'+dest_split[-1]
        else:
            prefix, counter, suffix = match.groups()
            counter = int(counter)+1
        assert counter < 100, dest+': Too many duplicates!'
        dest = insert_counter(prefix, counter, suffix)
    if do_warn:
        LOGGER.warning('Duplicate file name %s changed to %s', orig_dest, dest)
    return dest


def insert_counter(prefix, counter, suffix):
    """Insert the counter into the destination"""
    return prefix+'{:02}'.format(counter)+suffix


__all__ = ['get_unique_dest']


if __name__ == '__main__':
    phorg.setup_logging(verbose=True)
    LOGGER.info(get_unique_dest('/tmp/phorg/a.jpg'))
    LOGGER.info(get_unique_dest('/tmp/phorg/b.jpg'))
    LOGGER.info(get_unique_dest('/tmp/phorg/c.jpg'))
