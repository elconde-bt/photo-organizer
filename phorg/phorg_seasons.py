"""Tools for determining what season a date is in"""
import datetime
import logging
import phorg
import pytz
import csv

LOGGER = logging.getLogger(__name__)
TIME_ZONE = pytz.timezone("America/New_York")


def get_season(py_datetime):
    """Given a datetime what season is it? Assume New York time zone.
    Return a string like this:

    1999-1-winter
    1999-2-spring
    1999-3-summer
    1999-4-autumn

    We using astrological definitions of seasons. If it's december after
    the solstice group it in next year's winter.

    E.g. 2010-12-30 -> 2011-1-winter

    """
    py_datetime_nyc: datetime.datetime = TIME_ZONE.localize(py_datetime)
    season_boundaries_by_year = get_season_boundaries_by_year()
    year = py_datetime_nyc.year
    for i, boundary in enumerate(season_boundaries_by_year[year]):
        if boundary > py_datetime_nyc:
            season = i + 1
            break
    else:
        # Late december. Use next year's season
        year += 1
        season = 1
    return '{}-{}-{}'.format(
        year, season, {
            1: 'winter', 2: 'spring', 3: 'summer', 4: 'autumn'
        }[season]
    )


def get_season_boundaries_by_year():
    """What are the season boundaries per year. Return a dictionary.
    Keys are years. Each value is a list of season boundaries:
    - March equinox
    - June solstice
    - September equinox
    - December solstice"""
    lines = get_lines_from_seasons_file()
    season_boundaries_by_year = {}
    for line in lines:
        year = int(line[0])
        season_boundaries = []
        for month_position in range(1, 9, 2):
            format_string = '{} {{{}}} {{{}}}'.format(
                year, month_position, month_position + 1
            )
            date_string = format_string.format(*line)
            date_string = date_string[:-4]  # Don't need EST/EDT
            season_boundaries += [
                TIME_ZONE.localize(
                    datetime.datetime.strptime(
                        date_string, '%Y %b %d %I:%M %p'
                    )
                )
            ]
        season_boundaries_by_year[year] = season_boundaries
    return season_boundaries_by_year


def get_lines_from_seasons_file():
    """Get all the lines from the seasons file excluding comments."""
    with open('seasons.tsv', 'r') as seasons_file:
        reader = csv.reader(seasons_file, delimiter='\t')
        lines = []
        for line in reader:
            if not line:
                continue
            if line[0].startswith('#'):
                continue
            lines += [line]
    return lines


__all__ = ['get_season']

if __name__ == '__main__':
    phorg.setup_logging(verbose=True)
