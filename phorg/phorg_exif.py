"""Tools for getting EXIF data from a file"""
import time
from typing import Optional, Any

import exifread
import logging
import phorg
import datetime

LOGGER = logging.getLogger(__name__)


def get_datetime(photo_file_full_name):
    with open(photo_file_full_name, 'rb') as photo_file:
        tags = exifread.process_file(photo_file, stop_tag='Image DateTime')
    image_date_time: exifread.classes.IfdTag = tags.get('Image DateTime')
    if not image_date_time:
        return
    image_date_time_value = image_date_time.values
    try:
        strptime = time.strptime(image_date_time_value, '%Y:%m:%d %H:%M:%S')
    except ValueError:
        LOGGER.error(
            '%s: Strange time data "%s"', photo_file_full_name,
            image_date_time_value
        )
        return
    return datetime.datetime.fromtimestamp(time.mktime(strptime))


if __name__ == '__main__':
    phorg.setup_logging(verbose=True)
    date_ = get_datetime(
        '/home/elconde/Flickr-Download/farm1/783/DSC01731.jpg'
    )
    LOGGER.debug('%s %s', date_, type(date_))

__all__ = ['get_datetime']
