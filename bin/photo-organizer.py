#!/usr/bin/env python
"""Organizes photos into directories and removes duplicates."""
import argparse
import hashlib
import shutil
import sys
import phorg
import logging
import os
import time

LOGGER = logging.getLogger(__name__)

MD5_SUMS = {}


def parse_args():
    """Parse the command line arguments"""
    parser = argparse.ArgumentParser(description=__doc__.splitlines()[0])

    def directory(string_argument):
        """Expand user"""
        return os.path.expanduser(string_argument)
    default_ = ' (default: %(default)s)'
    parser.add_argument(
        '--input-dir', help='directory where the photos live.'+default_,
        default='~/Flickr-Download', type=directory
    )
    parser.add_argument(
        '--output-dir',
        help='directory where the organized photos will go.'+default_,
        default='~/Photos', type=directory
    )
    parser.add_argument(
        '--verbose', help='Verbose logging', action='store_true'
    )
    return parser.parse_args()


def get_full_names(input_dir):
    """Get all the full file names for every file in this directory,
    recursively."""
    for dirpath, dirnames, filenames in os.walk(input_dir):
        for file_name in filenames:
            full_name = os.path.join(dirpath, file_name)
            yield full_name


def initialize_md5_sums(output_dir):
    """Go through the output directory collecting md5 sums."""
    LOGGER.info('%s: Initializing MD5 sums', output_dir)
    last_logged_time = time.time()
    count = 0
    for full_name in get_full_names(output_dir):
        count += 1
        MD5_SUMS[get_md5_sum(full_name)] = full_name
        if time.time() - last_logged_time > 3:
            LOGGER.info('%d files summed', count)
            last_logged_time = time.time()


def photo_organizer(input_dir, output_dir):
    """Recurse through the photos copying them to the output
    directory."""
    initialize_md5_sums(output_dir)
    LOGGER.info('Copying files from %s to %s', input_dir, output_dir)
    for full_name in get_full_names(input_dir):
        copy_photo(full_name, output_dir)


def copy_photo(full_name, output_dir):
    """Copy a photo to the output directory"""
    md5_sum = get_md5_sum(full_name)
    if md5_sum in MD5_SUMS:
        LOGGER.warning(
            'Skipping %s: Duplicate with %s', full_name,
            MD5_SUMS[md5_sum]
        )
        return
    MD5_SUMS[md5_sum] = full_name
    date_dir_name = get_date_dir_name(full_name)
    dest_dir = os.path.join(output_dir, date_dir_name)
    dest = os.path.join(dest_dir, os.path.basename(full_name))
    dest = phorg.get_unique_dest(dest)
    LOGGER.debug('%s => %s', full_name, dest)
    if not os.path.isdir(dest_dir):
        os.makedirs(dest_dir)
    shutil.copy(full_name, dest)


def get_date_dir_name(full_name):
    """What should the date directory name be?"""
    photo_datetime = phorg.get_datetime(full_name)
    if not photo_datetime:
        return 'None'
    return phorg.get_season(photo_datetime)


def get_md5_sum(full_name):
    """Get the MD5 sum of a file"""
    with open(full_name, 'rb') as f_open:
        return hashlib.md5(f_open.read()).hexdigest()


def main():
    args = parse_args()
    phorg.setup_logging(args.verbose)
    photo_organizer(args.input_dir, args.output_dir)


if __name__ == '__main__':
    sys.exit(main())
